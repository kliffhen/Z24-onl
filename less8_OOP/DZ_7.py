import time
class auto:
    def __init__(self, brand, age, mark, weight, color):
        self.brand  = brand
        self.age = age 
        self.color = color
        self.mark = mark
        self.weight = weight
    
    def move(self):
        print("Move")
    def stop(self):
        print("stop")
    def birthday(self):
        self.age += 1

class truck(auto):
    def __init__(self, brand, age, mark, weight, color, max_load):
        super().__init__(brand, age, mark, weight, color)
        self.max_load = max_load
    def move(self):
        print("Attention")
        super().move()
    def load(self):
        time.sleep(1)
        print("load")
        time.sleep(1)

class car(auto):
    def __init__(self, brand, age, mark, weight, color, max_speed):
        super().__init__(brand, age, mark, weight, color)
        self.max_speed = max_speed
    def move(self):
        print("Attention")
        super().move()
        print(f"Max_speed is {self.max_speed}")

MAZ = truck("maz", 20, 1010, 5000, "brown", 3000)
VAZ = car("vaz", 17, 537, 1000, "green", 110)

MAZ.move()
MAZ.load()

VAZ.move()










