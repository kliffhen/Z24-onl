words = []
biggest_word_dict = {}
biggest_len = 0

with open('article.txt', 'r') as file:
    data = file.read()

lines = data.split('\n')           #divide into list of lines
words_in_lists = list(map(lambda x: x.split(' '), lines))  #every list split into words 

for list in words_in_lists:   #add each word in list in lowercase
    for word in list:
        words.append(word.lower())

for word in words:              #find length of biggest word
    if len(word) > biggest_len:
        biggest_len = len(word)

for word in words:              #add longest_words to dict
    if len(word) == biggest_len:
        biggest_word_dict[word] = 0

for word in words:              #count the amount of all the longest words if there are several with same length
    if len(word) == biggest_len:
        biggest_word_dict[word] += 1     

print(f'Длина самого длинного слова в файле составляет: {biggest_len}')
print('Вот список самых длинных слов:')

for word, num in biggest_word_dict.items():     #print all the longest words
    print([word] * num)

# for word in words_list:
#     words_dict[word] = 0

# for word in words_list:
#     words_dict[word] += 1



# print(words_dict)
   



#print(data)